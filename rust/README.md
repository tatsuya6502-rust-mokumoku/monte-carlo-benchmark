


```console
$ cargo run --release --bin main1
times:1
pi:3.14197
time:0.16973585
times:2
pi:3.1420205
time:0.17213263
...

times:100
pi:3.1417997
time:0.17152506
AVE:0.17096137
```

```console
$ cargo run --release --bin main2
times:1
pi:3.142059
time:0.08354444
times:2
pi:3.142779
time:0.08258054
...

times:100
pi:3.1419091
time:0.08419315
AVE:0.08502699
```

- Fedora 32 (Linux x86_64)
- Intel Core i5-7200U CPU @ 2.50GHz
- Rust 1.44.1
